class AddRemainingHours < ActiveRecord::Migration
 
  def self.up
    unless column_exists? :issues, :remaining_hours
      add_column(:issues, "remaining_hours", :float)
    end
  end

  def self.down
    if column_exists? :issues, :remaining_hours
      remove_column(:issues, "remaining_hours")
    end
  end
  
end