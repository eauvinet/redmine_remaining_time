class AddDrift < ActiveRecord::Migration
 
  def self.up
    add_column(:issues, "drift", :float)
  end

  def self.down
    remove_column(:issues, "drift")
  end
  
end