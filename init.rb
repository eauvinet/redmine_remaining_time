
Redmine::Plugin.register :redmine_remaining_time do
  name 'Redmine Remaining Time plugin'
  author 'Emmanuel Auvinet'
  description 'This plugin adds remaining time functionnality to issues'
  version '0.0.1'
  url 'https://bitbucket.org/eauvinet/redmine_remaining_time'

  settings(:default => {
  'hours_in_day' => '7',
  'char_for_day' => 'd',
  
  'days_in_week' => '5',
 
  'default_unit' => 'hours',
  
  }, :partial => 'settings/remaining_time_settings')
  
end

ActionDispatch::Callbacks.to_prepare do
	IssuesController.send :helper, 'issue_remaining_time'
	
	unless Issue.included_modules.include? RedmineRemainingTime::IssuePatch
   	Issue.send(:include, RedmineRemainingTime::IssuePatch)
  end

  unless Project.included_modules.include? RedmineRemainingTime::ProjectPatch
    Project.send(:include, RedmineRemainingTime::ProjectPatch)
  end

  unless Redmine::I18n.included_modules.include? RedmineRemainingTime::I18nPatch
    Redmine::I18n.send(:include, RedmineRemainingTime::I18nPatch)
  end

  unless Redmine::CoreExtensions::String::Conversions.included_modules.include? RedmineRemainingTime::ConversionsPatch
    Redmine::CoreExtensions::String::Conversions.send(:include, RedmineRemainingTime::ConversionsPatch)
  end

  unless IssueQuery.included_modules.include? RedmineRemainingTime::IssueQueryPatch
    IssueQuery.send(:include, RedmineRemainingTime::IssueQueryPatch)
  end

  unless QueriesHelper.included_modules.include? RedmineRemainingTime::QueriesHelperPatch
    QueriesHelper.send(:include, RedmineRemainingTime::QueriesHelperPatch)
  end

  unless SettingsHelper.included_modules.include? RedmineRemainingTime::SettingsHelperPatch
    SettingsHelper.send(:include, RedmineRemainingTime::SettingsHelperPatch)
  end
  
  unless TimeEntry.included_modules.include? RedmineRemainingTime::TimeEntryPatch
    TimeEntry.send(:include, RedmineRemainingTime::TimeEntryPatch)
  end

  unless Version.included_modules.include? RedmineRemainingTime::VersionPatch
    Version.send(:include, RedmineRemainingTime::VersionPatch)
  end
  
  unless Tracker.included_modules.include? RedmineRemainingTime::TrackerPatch
    Tracker.send(:include, RedmineRemainingTime::TrackerPatch)
  end


end

require 'redmine_remaining_time/hooks/views_issues_hook'
require 'redmine_remaining_time/hooks/views_projects_hook'
require 'redmine_remaining_time/hooks/views_timelog_hook'
require 'redmine_remaining_time/hooks/views_versions_hook'
require 'redmine_remaining_time/hooks/controller_timelog_hook'
