require_dependency 'issue'

module RedmineRemainingTime
  module IssuePatch

  	def self.included(base)
      base.send(:include, InstanceMethods)

      base.class_eval do
        safe_attributes 'remaining_hours'
        validates :remaining_hours, :numericality => {:greater_than_or_equal_to => 0, :allow_nil => true, :message => :invalid}
        before_save :compute_drift, :update_done_ratio_from_remaining_time
        alias_method_chain :safe_attribute_names, :remaining
        alias_method_chain :reload, :remaining
      end
     
    end
    
    module InstanceMethods
 
 
      def reload_with_remaining(*args)
        @total_remaining_hours = nil
        reload_without_remaining(*args)
      end

      def update_done_ratio_from_remaining_time
        if done_ratio_remaining?
          # done ratio = remaining time / total time
          unless Issue.use_status_for_done_ratio? && status && status.default_done_ratio
            self.done_ratio = compute_done_ratio_for(self.total_spent_hours,self.total_remaining_hours)
          end
        end
      end
    
      def safe_attribute_names_with_remaining(user=nil)
        names = safe_attribute_names_without_remaining
        if done_ratio_remaining?
          names -= %w(done_ratio)
        end
        names
      end
  
      def done_ratio_remaining?
         Setting.parent_issue_done_ratio == 'remaining'
      end

      def total_remaining_hours
	      if leaf?
	        remaining_hours
	      else
		      @total_remaining_hours ||= self_and_descendants.sum(:remaining_hours)
 	      end
  	  end
      
      def remaining_hours=(h)
        write_attribute :remaining_hours, (h.is_a?(String) ? h.to_hours : h)
      end

      def compute_drift
        write_attribute :drift, (estimated_hours || 0) - (remaining_hours || 0) - (spent_hours || 0)
      end

      def total_drift
        if leaf?
           drift
        else
           @total_drift ||= self_and_descendants.sum(:drift)
        end
      end
      
      def compute_done_ratio_for(spent_hours,remaining_hours)
         total_time = (remaining_hours || 0) + (spent_hours || 0)
         ratio = 0
         if(total_time > 0)
           ratio = (spent_hours || 0) * 100 / total_time
         end
         ratio
      end
      
      def done_ratio=(r)
        write_attribute :done_ratio, (r || 0)
      end
    end
  end
end