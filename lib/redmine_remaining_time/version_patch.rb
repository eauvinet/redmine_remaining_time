module RedmineRemainingTime
  module VersionPatch

  	def self.included(base)
      base.class_eval do
      
      end
      base.send(:include, InstanceMethods)
      
    end

    module InstanceMethods

      def remaining_hours
	      @remaining_hours ||= fixed_issues.sum(:remaining_hours).to_f
  	  end

    end
  end
end