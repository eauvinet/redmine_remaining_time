module RedmineRemainingTime
  module TrackerPatch
	def self.included(base) # :nodoc:

	  base.class_eval do
        core_fields = remove_const(:CORE_FIELDS)
        core_fields_all = remove_const(:CORE_FIELDS_ALL)
        # Fields that can be disabled
        # Other (future) fields should be appended, not inserted!
        const_set(:CORE_FIELDS, (core_fields + %w(remaining_hours)).freeze)
        const_set(:CORE_FIELDS_ALL, (core_fields_all + core_fields).freeze)
	  end
    end
  end
end
