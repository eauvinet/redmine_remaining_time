module RedmineRemainingTime
  module ProjectPatch

  	def self.included(base)
      base.send(:include, InstanceMethods)
      
    end   

    module InstanceMethods

		def remaining_hours
		  @remaining_hours ||= issues.sum(:remaining_hours).to_f
		end

    def estimated_hours 
      @estimated_hours ||= issues.sum(:estimated_hours).to_f
    end
	end
  end
end