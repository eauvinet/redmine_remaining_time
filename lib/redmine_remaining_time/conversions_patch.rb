
module RedmineRemainingTime
  module ConversionsPatch
	def self.included(base) # :nodoc:

	  base.send(:include, InstanceMethods)

	  base.class_eval do
	    unloadable # Send unloadable so it will not be unloaded in development

	    alias_method :to_hours, :to_extended_hours

	  end
    end

      module InstanceMethods
  		# Parses hours format and returns a float
	    def to_extended_hours
	      day_char = Setting.plugin_redmine_remaining_time['char_for_day']
	      value = 0
	      s = self.dup
	      s.strip!
	      if s =~ %r{^(\d+([.,]\d+)?)h?$}
	        s = $1
	         # 2,5 => 2.5
	         s.gsub!(',', '.')
	        begin; value = Kernel.Float(s); rescue; value = nil; end
	      elsif 
	      	s =~ %r{^(\d+([.,]\d+)?)#{day_char}?$}
	        s = $1
	        s.gsub!(',', '.')
	        begin
	        	 value = Kernel.Float(s) * Setting.plugin_redmine_remaining_time['hours_in_day'].to_f
	        rescue
	         value = nil
	        end
	      else
	        # 2:30 => 2.5
	        s.gsub!(%r{^(\d+):(\d+)$}) { $1.to_i + $2.to_i / 60.0 }
	        # 2h30, 2h, 30m => 2.5, 2, 0.5
	        s.gsub!(%r{^((\d+)\s*(h|hours?))?\s*((\d+)\s*(m|min)?)?$}i) { |m| ($1 || $4) ? ($2.to_i + $5.to_i / 60.0) : m[0] }
	        
	        s.gsub!(',', '.')
	        begin; value = Kernel.Float(s); rescue; value = nil; end
	      end
	     
	  
	      
	      value
   		end
      end

  end
end
