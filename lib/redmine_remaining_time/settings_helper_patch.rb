module RedmineRemainingTime
  module SettingsHelperPatch

  	def self.included(base)
      
      base.send(:include, InstanceMethods)
      base.class_eval do
         alias_method_chain :parent_issue_done_ratio_options, :remaining
      end
    end

    module InstanceMethods
    
      def parent_issue_done_ratio_options_with_remaining
        options = parent_issue_done_ratio_options_without_remaining
        options << [l(:label_parent_task_attributes_remaining), 'remaining']
      end
    end
  end
end