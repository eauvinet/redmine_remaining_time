
module RedmineRemainingTime
  module IssueQueryPatch

  	def self.included(base)
  	
      base.send(:include, InstanceMethods)

      base.class_eval do
         base.available_columns << QueryColumn.new(:remaining_hours, :sortable => "#{Issue.table_name}.remaining_hours")
         base.available_columns << QueryColumn.new(:total_remaining_hours, :sortable => "COALESCE((SELECT SUM(remaining_hours) FROM #{Issue.table_name} subtasks" +
        " WHERE subtasks.root_id = #{Issue.table_name}.root_id AND subtasks.lft >= #{Issue.table_name}.lft AND subtasks.rgt <= #{Issue.table_name}.rgt), 0)", :default_order => 'desc')
         base.available_columns << QueryColumn.new(:drift, :sortable => "#{Issue.table_name}.drift")
          base.available_columns << QueryColumn.new(:total_drift, :sortable => "COALESCE((SELECT SUM(drift) FROM #{Issue.table_name} subtasks" +
        " WHERE subtasks.root_id = #{Issue.table_name}.root_id AND subtasks.lft >= #{Issue.table_name}.lft AND subtasks.rgt <= #{Issue.table_name}.rgt), 0)", :default_order => 'desc')
         alias_method_chain :initialize_available_filters, :remaining_hours
      end
      
    end

    module InstanceMethods

    	def initialize_available_filters_with_remaining_hours
    		initialize_available_filters_without_remaining_hours
    		add_available_filter "remaining_hours", :type => :float
    		add_available_filter "drift", :type => :float
    	end
    end

  end
end
