module RedmineRemainingTime
  module Hooks
    class ViewsVersionsHook < Redmine::Hook::ViewListener
      render_on :view_versions_show_bottom, partial: 'versions/remaining_time_show'
    end
  end
end

