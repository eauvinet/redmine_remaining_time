module RedmineRemainingTime
  module Hooks
    class ControllerTimelogHook < Redmine::Hook::ViewListener

      def controller_timelog_edit_before_save(context = {})
      	params = context[:params]
        time_entry = context[:time_entry]
        if time_entry.issue_id.present? 
          if params[:time_entry][:update_remaining] == "1"
         	  remaining_hours = (time_entry.issue.remaining_hours || 0) - (time_entry.hours || 0)  
         	  if remaining_hours < 0
	         	  remaining_hours = 0
         	  end
            time_entry.issue.remaining_hours =  remaining_hours
          end
        end
      end

    end
  end
end