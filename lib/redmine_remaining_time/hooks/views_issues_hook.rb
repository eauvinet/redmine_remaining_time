module RedmineRemainingTime
  module Hooks
    class ViewsIssuesHook < Redmine::Hook::ViewListener
      render_on :view_issues_show_details_bottom, partial: 'issues/remaining_time_show_details'
      render_on :view_issues_form_details_bottom, partial: 'issues/remaining_time_form'
    end
  end
end
