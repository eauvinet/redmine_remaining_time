module RedmineRemainingTime
  module Hooks
    class ViewsProjectsHook < Redmine::Hook::ViewListener
      render_on :view_projects_show_sidebar_bottom, partial: 'projects/remaining_time_sidebar'
    end
  end
end
