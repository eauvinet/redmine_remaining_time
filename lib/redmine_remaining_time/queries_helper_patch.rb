module RedmineRemainingTime
  module QueriesHelperPatch

  	def self.included(base)
      
      base.send(:include, InstanceMethods)
      base.class_eval do
         alias_method_chain :column_value, :hours
      end
    end

    module InstanceMethods

      def column_value_with_hours(column, issue, value)
        
        case column.name
        when :hours #time_entry.hours
          l_hours_short(issue.hours)
        when :estimated_hours
          l_hours_short(issue.estimated_hours)
        when :total_estimated_hours
          l_hours_short(issue.total_estimated_hours)
        when :remaining_hours
          l_hours_short(issue.remaining_hours)
        when :total_remaining_hours
          l_hours_short(issue.total_remaining_hours)
        when :spent_hours
          l_hours_short(issue.spent_hours)
        when :total_spent_hours
          l_hours_short(issue.total_spent_hours)
        when :drift
          l_hours_short(issue.drift)
        when :total_drift
          l_hours_short(issue.total_drift)
        else
          column_value_without_hours(column,issue,value)
        end
      end

    end
  end
end