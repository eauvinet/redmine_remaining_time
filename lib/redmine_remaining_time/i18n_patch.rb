module RedmineRemainingTime
  module I18nPatch
	def self.included(base) # :nodoc:
	  base.send(:include, InstanceMethods)
	 
	  base.class_eval do
	    unloadable # Send unloadable so it will not be unloaded in development
	   
	    alias_method :l_hours, :l_extended_hours
	    alias_method :l_hours_short, :l_extended_hours_short

	  end
	end

    module InstanceMethods

      def l_extended_hours(hours)
	    f_hours = hours.to_f
	    f_hours_per_day = Setting.plugin_redmine_remaining_time['hours_in_day'].to_f
	  	f_days = f_hours / f_hours_per_day
	  	if Setting.plugin_redmine_remaining_time['default_unit'] == 'days'
	  	  l((f_days < 2.0 ? :label_f_day : :label_f_day_plural), :value => ("%.2f" % f_days))
	  	elsif Setting.plugin_redmine_remaining_time['default_unit'] == 'hours'
	  	  l((f_hours < 2.0 ? :label_f_hour : :label_f_hour_plural), :value => ("%.2f" % f_hours))
	  	end
	  end

	  def l_extended_hours_short(hours)
      f_hours = hours.to_f
	  	f_hours_per_day = Setting.plugin_redmine_remaining_time['hours_in_day'].to_f
	  	f_days = f_hours / f_hours_per_day
	  	if Setting.plugin_redmine_remaining_time['default_unit'] == 'days'
	  		l(:label_f_short_days, :value => ("%.2f" % f_days))
	  	elsif Setting.plugin_redmine_remaining_time['default_unit'] == 'hours'
	      l(:label_f_short_hours, :value => ("%.2f" % f_hours))
	    end
	  end



    end
  end
end
