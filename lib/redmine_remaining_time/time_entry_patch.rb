module RedmineRemainingTime
  module TimeEntryPatch

  	def self.included(base)
      base.class_eval do
        after_save :update_issue
      end
      base.send(:include, InstanceMethods)
      
    end

    module InstanceMethods

      def update_issue
          issue.save unless issue.nil?
      end
      def update_remaining
	      return false if issue.nil?
        return true
  	  end

    end
  end
end