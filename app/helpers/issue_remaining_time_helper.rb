module IssueRemainingTimeHelper  

  def issue_remaining_hours_details(issue)
    if issue.total_remaining_hours.present?
      if issue.total_remaining_hours == issue.remaining_hours
        l_hours_short(issue.remaining_hours)
      else
        s = issue.remaining_hours.present? ? l_hours_short(issue.remaining_hours) : ""
        s << " (#{l(:label_total)}: #{l_hours_short(issue.total_remaining_hours)})"
        s.html_safe
      end
    end
  end
  
  def render_estimated_time(issue)
    value = issue.total_estimated_hours.to_f || 0
    estimated_time = issue.total_estimated_hours.to_f || 0 
    remaining_time = issue.total_remaining_hours.to_f || 0 
    logged_time = issue.total_spent_hours.to_f || 0 
    total_time = [estimated_time, logged_time + remaining_time].max
    estimated_percent = 100
    if total_time > 0
      estimated_percent = value * 100 / total_time 
    end
    remaining_percent = 100 - estimated_percent
   
    content_tag(:table,content_tag(:tr,
        (estimated_percent > 0 ? content_tag(:td, '', :class => ['timebar','estimated'],:style => "width:#{estimated_percent}%;"): "").html_safe +
        (remaining_percent > 0 ? content_tag(:td, '', :class => ['timebar','drift'],:style => "width:#{remaining_percent}%;"): "").html_safe
      ),:class => ['timebar-table']).html_safe +
      content_tag('p', l_hours_short(value), :class => 'timevalue').html_safe
  end

  def render_remaining_time(issue)
    value = issue.total_remaining_hours || 0
    estimated_time = issue.total_estimated_hours || 0 
    remaining_time = issue.total_remaining_hours || 0 
    logged_time = issue.total_spent_hours || 0 
    total_time = [estimated_time, logged_time + remaining_time].max
    remaining_percent = 100
    if total_time > 0
      remaining_percent = value * 100 / total_time 
    end
    logged_percent = 100 - remaining_percent

  	content_tag(:table,
  		content_tag(:tr,
         (logged_percent > 0 ? content_tag(:td, '', :class => ['timebar'], :style => "width:#{logged_percent}%;") : "").html_safe +
         (remaining_percent > 0 ? content_tag(:td, '',:class => ['timebar','remaining'], :style => "width:#{remaining_percent}%;") : "" ).html_safe
      ),:class => ['timebar-table']).html_safe +
      content_tag('p', l_hours_short(value), :class => 'timevalue').html_safe
  end

  def render_logged_time(issue)
	value = issue.total_spent_hours
  estimated_time = issue.total_estimated_hours || 0 
  remaining_time = issue.total_remaining_hours || 0 
  logged_time = issue.total_spent_hours || 0 
	total_time = [estimated_time, logged_time + remaining_time].max
  logged_percent = 0
  if total_time > 0
	  logged_percent =  value * 100 / total_time 
  end
	remaining_percent = 100 - logged_percent
   
  	content_tag(:table,
  		content_tag(:tr,
         (logged_percent > 0 ? content_tag(:td, '', :class => ['timebar','logged'],:style => "width:#{logged_percent}%;") : "" ).html_safe +
         (remaining_percent > 0 ? content_tag(:td, '',:class => ['timebar'], :style => "width:#{remaining_percent}%;"): "").html_safe
      ), :class => ['timebar-table']).html_safe +
      content_tag('p', link_to(l_hours_short(issue.total_spent_hours), issue_time_entries_path(issue)), :class => 'timevalue').html_safe
  end

end